import java.io.*;
import java.util.*;
import java.math.*;

public class panelize {

	public static String FS = null;
	public static String MO = null;

	public class Definition {
		int num;
		String def;
	}
	
	public class Macro {
		String name;
		ArrayList<String> defs;
	}
		
	public class Aperture {
		HashMap<Integer, Definition> definitions;
		HashMap<String, Macro> macros;
		int largestDef = -1;
		int aref = 0;
		
		public Aperture () {
			definitions = new HashMap<Integer, Definition>();
			macros = new HashMap<String, Macro>();
		}
	}
	
	public class Drill {
		HashMap<Integer, Definition> definitions;
		int largestDef = -1;
		
		public Drill() {
			definitions = new HashMap<Integer, Definition>();
		}
	}
	
	public class Translation {
		HashMap<Integer, Integer> dtrans;
		HashMap<String, String> mtrans;
		
		public Translation() {
			dtrans = new HashMap<Integer, Integer>();
			mtrans = new HashMap<String, String>();
		}
	}

	public static String readNumber(String s, int st) {
		String n = "";
		while (st < s.length() && (s.charAt(st) >= '0' && s.charAt(st) <= '9' || s.charAt(st) == '.' || s.charAt(st) == '-')) {
			n = n + s.charAt(st);
			st++;
		}
		return n;
	}
	
	public class DrillFile {
		String filename;
		BigDecimal xOffset;
		BigDecimal yOffset;
		BufferedReader br;
		Drill drill;
		Translation translation;
		
		public DrillFile(String fn, BigDecimal x, BigDecimal y, Drill d) {
			filename = fn;
			xOffset = x;
			yOffset = y;
			try {
				br = new BufferedReader(new FileReader(filename));
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
			drill = d;
			translation = new Translation();
		}
		
		public void readDefinition(String line) {
			Definition d = new Definition();
			String num = panelize.readNumber(line, 1);
			d.num = Integer.parseInt(num);
			d.def = line.substring(1 + num.length());

			// Check for existing entry
			for (Map.Entry<Integer, Definition> e : drill.definitions.entrySet()) {
				if (d.def.equals(e.getValue().def)) {
					if (d.num == e.getKey()) {
						// There is an existing entry with the same number, so skip
						return;
					}
					// There is an existing entry with an other number, do translation
					translation.dtrans.put(d.num, e.getKey());
					return;
				}
			}

			// Check for existing number
			if (drill.definitions.containsKey(d.num)) {
				int newnum = drill.largestDef + 1;
				translation.dtrans.put(d.num, newnum);
				d.num = newnum;
			}
			drill.definitions.put(d.num, d);
			if (d.num > drill.largestDef) drill.largestDef = d.num;
		}
		
		public void readHeader() {
			try {
				String line;
				while ((line = br.readLine()) != null) {
					if (line.startsWith("M48") || line.startsWith("M71,TZ") || line.startsWith("FMAT,2")) {
						continue;
					}
					else if (line.startsWith("T")) readDefinition(line); // Definition line
					else if (line.startsWith("%")) break; // This is the last line
					else {
						System.err.format("Unsupported header line: %s\n", line);
						System.exit(-3);
					}
				}	
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}

		public void writeBody(BufferedWriter w) {
			try {
				String line;
				while ((line = br.readLine()) != null) {
					if (line.startsWith("T00")) {
						// end of the file
						return;
					}
					else if (line.startsWith("T")) {
						int dnum = Integer.parseInt(line.substring(1,line.length()));
						if (translation.dtrans.containsKey(dnum)) {
							dnum = translation.dtrans.get(dnum);
						}
						w.write("T" + dnum + "\n");
					}
					else if (line.startsWith("X") || line.startsWith("Y")) {
						// Move with offset
						int xp = line.indexOf("X") + 1;
						String xs = panelize.readNumber(line, xp);
						int yp = line.indexOf("Y") + 1;
						String ys = panelize.readNumber(line, yp);						
						w.write("X" + (new BigDecimal(xs).add(xOffset)) + "Y" + (new BigDecimal(ys).add(yOffset)) + "\n");
					}
					else w.write(line + "\n");
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
		
	}
	
	public class GerberFile {
	
		String filename;
		Integer xOffset;
		Integer yOffset;
		BufferedReader br;
		Aperture aperture;
		Translation translation;
		int ref;
	
		public GerberFile(String fn, Integer x, Integer y, Aperture a) {
			filename = fn;
			xOffset = x;
			yOffset = y;
			try {
				br = new BufferedReader(new FileReader(filename));
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
			aperture = a;
			translation = new Translation();
			ref = a.aref;
			a.aref++;
		}
	
		public void readMacro(String line) {
			String name = line.substring(3, line.length()-1);
			Macro m = new Macro();
			m.name = name;
			m.defs = new ArrayList<String>();
			try {
				String l;
				while ((l = br.readLine()) != null) {
					m.defs.add(l);
					if (l.endsWith("%")) {
						break;
					}
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
			
			// Check for existing macro name
			if (aperture.macros.containsKey(m.name)) {
				ArrayList<String> defsx = aperture.macros.get(m.name).defs;
				if (m.defs.equals(defsx)) {
					// Same name, same content
					return;
				} else {
					// Same name, but different content. Name should be changed
					String newname = m.name + "-" + ref;
					translation.mtrans.put(m.name, newname);
					m.name = newname;
				}
			}
			aperture.macros.put(m.name, m);
		}
		
		public void readDefinition(String line) {
			Definition d = new Definition();
			String num = panelize.readNumber(line, 4);
			d.num = Integer.parseInt(num);
			d.def = line.substring(4 + num.length());

			// Check for existing entry
			for (Map.Entry<Integer, Definition> e : aperture.definitions.entrySet()) {
				if (d.def.equals(e.getValue().def)) {
					if (d.num == e.getKey()) {
						// There is an existing entry with the same number, so skip
						return;
					}
					// There is an existing entry with an other number, do translation
					translation.dtrans.put(d.num, e.getKey());
					return;
				}
			}

			// Check for existing number
			if (aperture.definitions.containsKey(d.num)) {
				int newnum = aperture.largestDef + 1;
				translation.dtrans.put(d.num, newnum);
				d.num = newnum;
			}
			aperture.definitions.put(d.num, d);
			if (d.num > aperture.largestDef) aperture.largestDef = d.num;
		}
	
		public void readHeader() {
			try {
				String line;
				while ((line = br.readLine()) != null) {
					//System.out.println("Debug rH: " + line);
					if (line.startsWith("%")) {
						if (line.startsWith("%FS")) {
							if (FS == null) {
								FS = line;
							} else {
								if (!FS.equals(line)) {
									System.err.println("Headers are different!");
									System.exit(-2);
								}
							}
						}
						else if (line.startsWith("%MO")) {
							if (MO == null) {
								MO = line;
							} else {
								if (!MO.equals(line)) {
									System.err.println("Headers are different!");
									System.exit(-2);
								}
							}
						}							
						else if (line.startsWith("%AM")) readMacro(line);
						else if (line.startsWith("%AD")) readDefinition(line);
					}
					if (!line.startsWith("%")) {
						br.reset();
						break;
					}
					br.mark(8192);
				}	
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}

		public void writeBody(BufferedWriter w) {
			try {
				String line;
				while ((line = br.readLine()) != null) {
					//System.out.println("Debug wB: "+line);
					if (line.startsWith("M02")) {
						// end of the file
						return;
					}
					else if (line.startsWith("D")) {
						int dnum = Integer.parseInt(line.substring(1,line.length()-1));
						if (translation.dtrans.containsKey(dnum)) {
							dnum = translation.dtrans.get(dnum);
						}
						w.write("D" + dnum + "*\n");
					}
					else if (line.startsWith("X") || line.startsWith("Y")) {
						// Move with offset
						int xp = line.indexOf("X") + 1;
						String xs = panelize.readNumber(line, xp);
						int yp = line.indexOf("Y") + 1;
						String ys = panelize.readNumber(line, yp);
						w.write("X" + (Integer.valueOf(xs) + xOffset) + "Y" + (Integer.valueOf(ys) + yOffset) + line.substring(yp+ys.length()) + "\n");
					}
					else w.write(line + "\n");
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
	}
	
    public static void main(String[] args) {
		panelize m = new panelize();
		m.start(args);
	}
	
	public void start(String[] args) {
		if (args.length < 4) {
			System.out.println("Usage: java panelize [-drill] <input> <Xoffset> <Yoffset> [...] <output>");
			System.out.println("All offset are in micrometer!");
			System.exit(-1);
		}
		
		Aperture a = new Aperture();
		Drill d = new Drill();

		int c = 0;
		int drill = 0;
		ArrayList<GerberFile> gfs = new ArrayList<GerberFile>();
		ArrayList<DrillFile> dfs = new ArrayList<DrillFile>();
		
		if (args[0].equals("-drill")) {
			drill = 1;
		}
		
		while (args.length > drill + (c + 1) * 3) {
			String filename = args[c * 3 + 0 + drill];
			if (drill == 0) {
				Integer xOffset = Integer.valueOf(args[c * 3 + 1 + drill]);
				Integer yOffset = Integer.valueOf(args[c * 3 + 2 + drill]);
				GerberFile gf = new GerberFile(filename, xOffset, yOffset, a);
				gfs.add(gf);
			} else {
				BigDecimal thsnd = new BigDecimal("1000");
				BigDecimal xOffset = new BigDecimal(args[c * 3 + 1 + drill]).divide(thsnd, 3, BigDecimal.ROUND_HALF_UP);
				BigDecimal yOffset = new BigDecimal(args[c * 3 + 2 + drill]).divide(thsnd, 3, BigDecimal.ROUND_HALF_UP);
				DrillFile df = new DrillFile(filename, xOffset, yOffset, d);
				dfs.add(df);
			}
			c++;
		}
		
		String ofname = args[c * 3 + drill];
		
		if (drill == 0) System.out.print("Placing gerber files ");
		else System.out.print("Placing drill files ");
			
		if (drill == 0) {
			for (GerberFile gf : gfs.toArray(new GerberFile[gfs.size()])) {
				System.out.print(gf.filename);
				System.out.print(", ");
				gf.readHeader();
			}
		} else {
			for (DrillFile df : dfs.toArray(new DrillFile[dfs.size()])) {
				System.out.print(df.filename);
				System.out.print(", ");
				df.readHeader();
			}		
		}
		System.out.println(" to " + ofname);

		// Open output file for writing
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(ofname));
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(-2);
		}
		
		// Write Header
		try {
			if (drill == 0) {
				// Gerber header
				writer.write(FS + "\n");
				writer.write(MO + "\n");
				
				for (String k : a.macros.keySet()) {
					writer.write("%AM" + k + "*" + "\n");
					Macro m = a.macros.get(k);
					for (String def : m.defs.toArray(new String[m.defs.size()])) {
						writer.write(def + "\n");
					}
				}
				for (Integer k : a.definitions.keySet()) {
					writer.write("%ADD" + k + a.definitions.get(k).def + "\n");
				}
			} else {
				// Drill header
				writer.write("M48\nM71,TZ\nFMAT,2\n");
				for (Integer k : d.definitions.keySet()) {
					writer.write("T" + k + d.definitions.get(k).def + "\n");
				}
				writer.write("%\n");
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(-2);
		}
		
		// Write bodies
		if (drill == 0) {
			for (GerberFile gf : gfs.toArray(new GerberFile[gfs.size()])) {
				gf.writeBody(writer);
			}
		} else {
			for (DrillFile df : dfs.toArray(new DrillFile[dfs.size()])) {
				df.writeBody(writer);
			}
		}
		
		// Close file
		try {
			if (drill == 0) {
				// Gerber file
				writer.write("M02*");
			} else {
				// Drill file
				writer.write("T00\nM30\n");
			}
			writer.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(-2);
		}
	}
}
#!/usr/bin/python

import zipfile
import ConfigParser
import sys
import os.path
import csv
import re
from subprocess import call

# getSubSize returns the size(x and y) of all subpanels
def getSubSize(subname):

    sx = 0
    sy = 0

    if subname in subs_attr:
        sx = subs_attr[subname]["sizex"]
        sy = subs_attr[subname]["sizey"]
        return (sx, sy)
        
    elif subname in parts:
        sx = parts_attr[subname]["sizex"]
        sy = parts_attr[subname]["sizey"]
        return (sx, sy)

    sub = config.get("Layout", subname)
    if re.match("V\(.*\)", sub):
        m = re.match("V\((.*)\)", sub)
        subsubs = m.group(1).split('+')
        for ss in subsubs:
            (x, y) = getSubSize(ss)
            if x > sx: sx = x
            sy = sy + y + 1000
        sy = sy - 1000
        
    elif re.match("H\(.*\)",sub):
        m = re.match("H\((.*)\)", sub)
        subsubs = m.group(1).split('+')
        for ss in subsubs:
            (x, y) = getSubSize(ss)
            if y > sy: sy = y
            sx = sx + x + 1000
        sx = sx - 1000
        
    else:
        (sx, sy) = getSubSize(subname)
        
    subs_attr[subname] = {}
    subs_attr[subname]["sizex"] = sx
    subs_attr[subname]["sizey"] = sy
                
    return (sx, sy)

# doPartLayout returns the layout tuples (part, x, y) in the pParts
def doPartLayout(pParts, separators, subexp, cx, cy):

    if subexp in parts:
        pParts.append( (subexp, cx, cy) )
        sx = parts_attr[subexp]["sizex"]
        sy = parts_attr[subexp]["sizey"]
        separators.append((cx - 500, cy - 500, cx + sx + 500, cy - 500))
        separators.append((cx + sx + 500, cy - 500, cx + sx + 500, cy + sy + 500))
        separators.append((cx + sx + 500, cy + sy + 500, cx - 500, cy + sy + 500))
        separators.append((cx - 500, cy + sy + 500, cx - 500, cy - 500))
        return (parts_attr[subexp]["sizex"], parts_attr[subexp]["sizey"])
    elif subexp in subs_attr:
        doPartLayout(pParts, separators, config.get("Layout", subexp), cx, cy)
        return (subs_attr[subexp]["sizex"], subs_attr[subexp]["sizey"])
    elif re.match("V\(.*\)", subexp):
        m = re.match("V\((.*)\)", subexp)
        subsubs = m.group(1).split('+')
        for ss in subsubs:
            (sx, sy) = doPartLayout(pParts, separators, ss, cx, cy)
            cy = cy + sy + 1000
    elif re.match("H\(.*\)", subexp):
        m = re.match("H\((.*)\)", subexp)
        subsubs = m.group(1).split('+')
        for ss in subsubs:
            (sx, sy) = doPartLayout(pParts, separators, ss, cx, cy)
            cx = cx + sx + 1000

    return (0,0)
            
config = ConfigParser.ConfigParser()
config.optionxform = str
config.read(sys.argv[1])

srcPath = config.get("Data", "SrcPath")
panelPath = config.get("Data", "PanelPath")

parts = {}
parts_attr = {}

pos = config.options("Parts")

for p in pos:
    parts[p] = config.get("Parts", p)

    # Create panel files if missing (0,0 alignment)
    if not os.path.isfile(os.path.join(panelPath, parts[p] + ".gtl")):
        print "Creating alignment for ", parts[p]
        with zipfile.ZipFile(os.path.join(srcPath, parts[p] + "_gerber.zip"), 'r') as zip:
            zip.extractall(panelPath)
        with zipfile.ZipFile(os.path.join(srcPath, parts[p] + "_ncdrill.zip"), 'r') as zip:
            zip.extractall(panelPath)
        with open(os.path.join(panelPath, "layers.cfg"), 'r') as layerfile:
            layerreader = csv.reader(layerfile)
            gerbers = {}
            for row in layerreader:
                gerbers[row[0]] = row[1].strip()
            with open(os.path.join(panelPath, gerbers["mechanical details"]), 'r') as gkofile:
                xoff = 0
                yoff = 0
                for cnt, line in enumerate(gkofile):
                    m = re.match("X([-0-9]*)Y([-0-9]*)", line)
                    if m != None:
                        if int(m.group(1)) < xoff: xoff = int(m.group(1))
                        if int(m.group(2)) < yoff: yoff = int(m.group(2))
            for gtag in gerbers:
                filename, file_extension = os.path.splitext(gerbers[gtag])
                call(["java", "-jar", "panelize.jar", os.path.join(panelPath, gerbers[gtag]), str(-xoff), str(-yoff), os.path.join(panelPath, parts[p] + file_extension)], stdout=open(os.devnull, 'wb'))
                #call(["java", "-jar", "panelize.jar", os.path.join(panelPath, gerbers[gtag]), str(-xoff), str(-yoff), os.path.join(panelPath, parts[p] + file_extension)])
                os.remove(os.path.join(panelPath, gerbers[gtag]))
    
            callparm = ["java", "-jar", "panelize.jar", "-drill"]
            if os.path.isfile(os.path.join(panelPath, "design_export-plated-all.drill")):
                callparm.extend((os.path.join(panelPath, "design_export-plated-all.drill"), str(-xoff), str(-yoff)))
            if os.path.isfile(os.path.join(panelPath, "design_export-unplated-all.drill")):
                callparm.extend((os.path.join(panelPath, "design_export-unplated-all.drill"), str(-xoff), str(-yoff)))
            callparm.append(os.path.join(panelPath, parts[p] + ".drill"))
            call(callparm, stdout=open(os.devnull, 'wb'))
            #call(["java", "-jar", "panelize.jar", "-drill", os.path.join(panelPath, "design_export-plated-all.drill"), str(-xoff), str(-yoff), os.path.join(panelPath, parts[p] + ".drill")])
            if os.path.isfile(os.path.join(panelPath, "design_export-plated-all.drill")):
                os.remove(os.path.join(panelPath, "design_export-plated-all.drill"))
            if os.path.isfile(os.path.join(panelPath, "design_export-unplated-all.drill")):
                os.remove(os.path.join(panelPath, "design_export-unplated-all.drill"))

    # Get dimensions
    with open(os.path.join(panelPath, parts[p] + ".gko"), 'r') as gkofile:
        maxx = 0
        maxy = 0
        for cnt, line in enumerate(gkofile):
            m = re.match("X([-0-9]*)Y([-0-9]*)", line)
            if m != None:
                if int(m.group(1)) > maxx: maxx = int(m.group(1))
                if int(m.group(2)) > maxy: maxy = int(m.group(2))
        parts_attr[p] = {}
        parts_attr[p]['sizex'] = maxx
        parts_attr[p]['sizey'] = maxy
        
print "Parts:"
for p in pos:
    print parts[p], parts_attr[p]['sizex'], parts_attr[p]['sizey']
    
# Calculate subpanel sizes (recursive)
subs = config.options("Layout")
subs_attr = {}

(mainx, mainy) = getSubSize("Main")

for s in subs:
    print s, subs_attr[s]["sizex"], subs_attr[s]["sizey"]
    
pParts = []
separators = []
doPartLayout(pParts, separators, "Main", 0, 0)
print pParts


# Distance form the starting point of a line
def distOnLine(cx, cy, line):
    (x1, y1, x2, y2) = line
    if cx == x1 and cx == x2:
        if cy >= y1 and cy <= y2: return cy - y1
        if cy >= y2 and cy <= y1: return cy - y2
    if cy == y1 and cy == y2:
        if cx >= x1 and cx <= x2: return cx - x1
        if cx >= x2 and cx <= x1: return cx - x2
    return -1

# Check the new hole. If it is on the same line of another hole and too close
# then it should not be there (False return)
def checkHole(cx, cy, s, holes):
    for (hx, hy) in holes:
        dh = distOnLine(hx, hy, s)
        if dh == -1: continue
        dc = distOnLine(cx, cy, s)
        if dc == -1:
            print "Internal Error!", s, hx, hy, dh, cx, cy, dc
            continue
        if abs(dh-dc) < 8000: return False
    return True

# Create the merged files
for ext in ["gbl", "gbo", "gbp", "gbs", "gko", "gtl", "gto", "gtp", "gts", "xln", "drill"]:
    callparm = ["java", "-jar", "panelize.jar"]
    if ext == "drill": callparm.append("-drill")
    for pp in pParts:
        callparm.append(os.path.join(panelPath, parts[pp[0]] + "." + ext))
        callparm.append(str(pp[1]))
        callparm.append(str(pp[2]))
    if ext == "xln" or ext == "drill":
        sepholes = []
        for s in separators:
            (x1, y1, x2, y2) = s
            # Add the end points, if they are not there yet
            if not (x1, y1) in sepholes and x1 > 0 and y1 > 0 and x1 < mainx and y1 < mainy:
                callparm.extend((os.path.join(panelPath, "drill1." + ext), str(x1), str(y1)))
                sepholes.append((x1, y1))
            if not (x2, y2) in sepholes and x2 > 0 and y2 > 0 and x2 < mainx and y2 < mainy: 
                callparm.extend((os.path.join(panelPath, "drill1." + ext), str(x2), str(y2)))
                sepholes.append((x2, y2))
            
            # Check new holes on the separator line (every 8 mm)
            if x1 == x2:
                dx = 0
                if y1 > y2:
                    temp = y1
                    y1 = y2
                    y2 = temp
                dy = 8000
            if y1 == y2:
                dy = 0
                if x1 > x2:
                    temp = x1
                    x1 = x2
                    x2 = temp
                dx = 8000
                
            tx = x1
            ty = y1
            while ty <= y2 and tx <= x2:
                if tx > 0 and ty > 0 and tx < mainx and ty < mainy and checkHole(tx, ty, s, sepholes):
                    callparm.extend((os.path.join(panelPath, "drill1." + ext), str(tx), str(ty)))
                    sepholes.append((tx, ty))
                tx = tx + dx
                ty = ty + dy
                        
    callparm.append("panel." + ext)
    p = call(callparm, stdout=open(os.devnull, 'wb'))
    #p = call(callparm)
    
# Merge outlines into top/bottom silkscreen
call(["java", "-jar", "panelize.jar", "panel.gto", "0", "0", "panel.gko", "0", "0", "panel_.gto" ], stdout=open(os.devnull, 'wb'))
call(["java", "-jar", "panelize.jar", "panel.gbo", "0", "0", "panel.gko", "0", "0", "panel_.gbo" ], stdout=open(os.devnull, 'wb'))
os.rename("panel_.gto", "panel.gto")
os.rename("panel_.gbo", "panel.gbo")

# Create new outline
gkofile = open("panel_.gko","w") 
gkofile.writelines([
    "%FSLAX33Y33*%\n", "%MOMM*%\n", "%ADD10C,0.381*%\n", "D10*\n", "%LNpath-0*%\n", "G01*\n", "X0Y0D02*\n",
    "X"+str(mainx)+"Y0D01*\n",
    "X"+str(mainx)+"Y"+str(mainy)+"*\n",
    "X0Y"+str(mainy)+"*\n",
    "X0Y0*\n", "%LNmechanical details_traces*%\n", "M02*\n" ])
os.rename("panel_.gko", "panel.gko")

# Create zip file
with zipfile.ZipFile("panel.zip", 'w') as zip:
    for ext in ["gbl", "gbo", "gbp", "gbs", "gko", "gtl", "gto", "gtp", "gts", "xln" ]:
        zip.write("panel." + ext)

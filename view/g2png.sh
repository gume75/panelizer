SRCZIP=/cygdrive/d/Temp/torolheto/SAS/src.zip

wd=`mktemp -d`
unzip -d "$wd" ${SRCZIP}/${1}_gerber.zip
fn=`head -1 ${wd}/layers.cfg | cut -d ' ' -f 2 | cut -d '.' -f 1`
xd=`pwd`

cd "${wd}"
${xd}/Gerber2pdf.exe -silentexit -nowarnings -output=tmp -combine -background=14,44,14 -colour=184,115,51 ${fn}.gts -colour=94,152,6,128 ${fn}.gtl -colour=255,255,255 ${fn}.gto -colour=230,232,250 ${fn}.gtp ${fn}.gko -colour=0,0,0 ${fn}.xln
/usr/bin/convert -density 600 -trim -quality 100 -verbose tmp.pdf ${xd}/${1}-top.png
rm tmp.pdf

${xd}/Gerber2pdf.exe -silentexit -nowarnings -output=tmp -combine -mirror -background=14,44,14 -colour=184,115,51 ${fn}.gbs -colour=94,152,6,128 ${fn}.gbl -colour=255,255,255 ${fn}.gbo -colour=230,232,250 ${fn}.gbp ${fn}.gko -colour=0,0,0 ${fn}.xln
/usr/bin/convert -density 600 -trim -quality 100 -verbose tmp.pdf ${xd}/${1}-bottom.png

cd "${xd}"
